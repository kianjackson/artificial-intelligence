import java.awt.Point;

public class AStarNode {
	private Point point;
	private int gValue;
	private int hValue;
	private int fValue;
	private int searchValue;
	private AStarNode parent;
	
	public AStarNode(Point point) {
		this.point = point;
		this.gValue = 0;
		this.hValue = 0;
		this.fValue = 0;
		this.searchValue = 0;
		this.parent = null;
	}
	
	public AStarNode(Point point, int gValue, int hValue, int fValue, int searchValue, AStarNode parent) {
		this.point = point;
		this.gValue = gValue;
		this.hValue = hValue;
		this.fValue = fValue;
		this.searchValue = searchValue;
		this.parent = parent;
	}
	
	/**
	 * @return the point
	 */
	public Point getPoint() {
		return point;
	}
	
	/**
	 * @param point the point to set
	 */
	public void setPoint(Point point) {
		this.point = point;
	}

	/**
	 * @return the gValue
	 */
	public int getgValue() {
		return gValue;
	}

	/**
	 * @param gValue the gValue to set
	 */
	public void setgValue(int gValue) {
		this.gValue = gValue;
	}

	/**
	 * @return the hValue
	 */
	public int gethValue() {
		return hValue;
	}
	
	/**
	 * @param hValue the hValue to set
	 */
	public void sethValue(int hValue) {
		this.hValue = hValue;
	}

	/**
	 * @return the fValue
	 */
	public int getfValue() {
		return fValue;
	}

	/**
	 * @param fValue the fValue to set
	 */
	public void setfValue(int fValue) {
		this.fValue = fValue;
	}

	/**
	 * @return the searchValue
	 */
	public int getSearchValue() {
		return searchValue;
	}

	/**
	 * @param searchValue the searchValue to set
	 */
	public void setSearchValue(int searchValue) {
		this.searchValue = searchValue;
	}

	/**
	 * @return the parent
	 */
	public AStarNode getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(AStarNode parent) {
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((point == null) ? 0 : point.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AStarNode))
			return false;
		AStarNode other = (AStarNode) obj;
		if (point == null) {
			if (other.point != null)
				return false;
		} else if (!point.equals(other.point))
			return false;
		return true;
	}
}
