
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Visualizer extends Application{
	private static GridPane mazePane;
	private static AStar.Cell[][] maze;
	private static Point agentPoint;
	private static int counter;
	private static ArrayDeque<AStarNode> pathStack;
	private static int numOfExpandedNodes;
	private static String filename;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		mazePane = new GridPane();	
		
		// Handle mouse events
		mazePane.setOnMouseClicked(event -> {
			addBlockingCell(event.getX(), event.getY());
		});
		
		// Handler for button events
		EventHandler<ActionEvent> buttonHandler = new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent event) {
		    	Button source = (Button)event.getSource();
		    	
		    	if(source.getText().equals("Load Maze")) {
		    		FileChooser fileChooser = new FileChooser();
		    		fileChooser.setTitle("Open Resource File");
		    		fileChooser.getExtensionFilters().add(new ExtensionFilter("Text Files", "*.txt"));
		    		File selectedFile = fileChooser.showOpenDialog(primaryStage);
		    		if (selectedFile != null) {
		    			filename = selectedFile.getName();
		    			maze = AStar.loadMaze(selectedFile.getAbsolutePath());
		    			agentPoint = AStar.getStartPoint(maze);
		    			drawMaze();
		    		}
	    		} else if(source.getText().equals("Repeated Forward A* (Larger g-value tie breaking)")) {
		    		if(maze == null) {
		    			Alert alert = new Alert(AlertType.ERROR, "Load a maze first.", ButtonType.OK);
		    			alert.showAndWait();
		    		} else {
		    			// Break ties by choosing nodes with larger g-values
		    			repeatedForwardAStar(new Comparator<AStarNode>() {
		    				@Override
		    				public int compare(AStarNode node1, AStarNode node2) {
		    					if(node1.getfValue() == node2.getfValue()) {
		    						return node2.getgValue() - node1.getgValue();
		    					}
		    					return node1.getfValue() - node2.getfValue();
		    				}
		    			});
		    		}
	    		} else if (source.getText().equals("Repeated Forward A* (Smaller g-value tie breaking)")) {
		    		if(maze == null) {
		    			Alert alert = new Alert(AlertType.ERROR, "Load a maze first.", ButtonType.OK);
		    			alert.showAndWait();
		    		} else {
		    			// Break ties by choosing nodes with smaller g-values
		    			repeatedForwardAStar(new Comparator<AStarNode>() {
		    				@Override
		    				public int compare(AStarNode node1, AStarNode node2) {
		    					if(node1.getfValue() == node2.getfValue()) {
		    						return node1.getgValue() - node2.getgValue();
		    					}
		    					return node1.getfValue() - node2.getfValue();
		    				}
		    			});
		    		}
		    	} else if (source.getText().equals("Repeated Backward A* (Larger g-value tie breaking)")) {
		    		if(maze == null) {
		    			Alert alert = new Alert(AlertType.ERROR, "Load a maze first.", ButtonType.OK);
		    			alert.showAndWait();
		    		} else {
		    			// Break ties by choosing nodes with larger g-values
		    			repeatedBackwardAStar(new Comparator<AStarNode>() {
		    				@Override
		    				public int compare(AStarNode node1, AStarNode node2) {
		    					if(node1.getfValue() == node2.getfValue()) {
		    						return node2.getgValue() - node1.getgValue();
		    					}
		    					return node1.getfValue() - node2.getfValue();
		    				}
		    			});
		    		}
		    	} else if (source.getText().equals("Adaptive A* (Larger g-value tie breaking)")) {
		    		if(maze == null) {
		    			Alert alert = new Alert(AlertType.ERROR, "Load a maze first.", ButtonType.OK);
		    			alert.showAndWait();
		    		} else {
		    			// Break ties by choosing nodes with larger g-values
		    			adaptiveAStar(new Comparator<AStarNode>() {
		    				@Override
		    				public int compare(AStarNode node1, AStarNode node2) {
		    					if(node1.getfValue() == node2.getfValue()) {
		    						return node2.getgValue() - node1.getgValue();
		    					}
		    					return node1.getfValue() - node2.getfValue();
		    				}
		    			});
		    		}
		    	}
		    }
		};
		
		// Scroll pane to hold the maze
		ScrollPane scrollPane = new ScrollPane(mazePane);	
		
		// Initialize Buttons
		Button loadButton = new Button("Load Maze");
		loadButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		loadButton.setOnAction(buttonHandler);
		
		Button forwardAStarLargerButton = new Button("Repeated Forward A* (Larger g-value tie breaking)");
		forwardAStarLargerButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		forwardAStarLargerButton.setOnAction(buttonHandler);
		
		Button forwardAStarSmallerButton = new Button("Repeated Forward A* (Smaller g-value tie breaking)");
		forwardAStarSmallerButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		forwardAStarSmallerButton.setOnAction(buttonHandler);
		
		Button backwardAStarLargerButton = new Button("Repeated Backward A* (Larger g-value tie breaking)");
		backwardAStarLargerButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		backwardAStarLargerButton.setOnAction(buttonHandler);
		
		Button adaptiveAStarButton = new Button("Adaptive A* (Larger g-value tie breaking)");
		adaptiveAStarButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		adaptiveAStarButton.setOnAction(buttonHandler);
		
		// Add Buttons to VerticalBox
		VBox buttonBox = new VBox(loadButton, forwardAStarLargerButton, forwardAStarSmallerButton, backwardAStarLargerButton, adaptiveAStarButton);
		
		// Add scrollPane and buttonBox to a Horizontal box
		HBox hBox = new HBox(scrollPane, buttonBox);
		
		// Display window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Scene scene = new Scene(hBox, screenSize.getWidth() / 1.5, screenSize.getHeight() / 1.5);

        primaryStage.setTitle("Maze Visualizer");
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.show();
	}
	
	/**
	 * This method will draw the path by following the parent pointers
	 * of each AStarNode
	 * @param goal the goal node of the path.
	 * @return a stack containing the path from the start node to the goal node
	 */
	private static ArrayDeque<AStarNode> drawPath(AStarNode goal, AStarNode start) {
		// goal is null if no path was found
		if(goal == null) {
			Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
			alert.showAndWait();
		}
		
		// Stack to hold the path. Top of stack will be start node and bottom will be goal node.
		ArrayDeque<AStarNode> pathStack = new ArrayDeque<AStarNode>();

		AStarNode curr = goal;
		
		// Draw the goal node in green
		Point p = curr.getPoint();
		Rectangle goalRect = new Rectangle(15, 15);
		goalRect.setFill(Color.GREEN);
		goalRect.setStroke(Color.LIGHTGREY);
		goalRect.setStrokeType(StrokeType.INSIDE);
		GridPane.setConstraints(goalRect, p.y, p.x);
		mazePane.getChildren().add(goalRect);
		
		// Draw the path with yellow outline
		while(curr.getParent() != null && !curr.getParent().equals(start)) {
			pathStack.push(curr);
			curr = curr.getParent();
			p = curr.getPoint();
			Rectangle rect = new Rectangle(15, 15);
			GridPane.setConstraints(rect, p.y, p.x);
			rect.setFill(Color.LIGHTGREY);
			rect.setStroke(Color.YELLOW);
			rect.setStrokeType(StrokeType.INSIDE);
			mazePane.getChildren().add(rect);
		}
		
		// Draw the start node in blue
		pathStack.push(curr);
		p = curr.getParent().getPoint();
		Rectangle startRect = new Rectangle(15, 15);
		startRect.setFill(Color.BLUE);
		startRect.setStroke(Color.LIGHTGREY);
		startRect.setStrokeType(StrokeType.INSIDE);
		GridPane.setConstraints(startRect, p.y, p.x);
		mazePane.getChildren().add(startRect);
		
		return pathStack;
	}
	
	/**
     * This method will draw the path by following the parent pointers
     * of each AStarNode BACKWARDS
     * @param goal the goal node of the path.
     * @return a stack containing the path from the start node to the goal node
     */
    private static ArrayDeque<AStarNode> drawPathBackwards(AStarNode goal, AStarNode start) {
        // goal is null if no path was found
        if(goal == null) {
            Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
            alert.showAndWait();
        }
        
        // Stack to hold the path. Top of stack will be start node and bottom will be goal node.
        ArrayDeque<AStarNode> pathStack = new ArrayDeque<AStarNode>();
        
        AStarNode curr = goal;
        
        // Draw the goal node in blue
        Point p = curr.getPoint();
        Rectangle goalRect = new Rectangle(15, 15);
        goalRect.setFill(Color.BLUE);
        goalRect.setStroke(Color.LIGHTGREY);
        goalRect.setStrokeType(StrokeType.INSIDE);
        GridPane.setConstraints(goalRect, p.y, p.x);
        mazePane.getChildren().add(goalRect);
        
        // Draw the path in red
        while(curr.getParent() != null && !curr.getParent().equals(start)) {
            curr = curr.getParent();
            pathStack.push(curr);
            p = curr.getPoint();
            Rectangle rect = new Rectangle(15, 15);
            GridPane.setConstraints(rect, p.y, p.x);
            rect.setFill(Color.LIGHTGREY);
            rect.setStroke(Color.YELLOW);
            rect.setStrokeType(StrokeType.INSIDE);
            mazePane.getChildren().add(rect);
            System.out.println(p);
        }
        
        // Draw the start node in green
        pathStack.push(curr);
        //pathStack.push(curr.getParent());
        p = curr.getParent().getPoint();
        Rectangle startRect = new Rectangle(15, 15);
        startRect.setFill(Color.GREEN);
        startRect.setStroke(Color.LIGHTGREY);
        startRect.setStrokeType(StrokeType.INSIDE);
        GridPane.setConstraints(startRect, p.y, p.x);
        mazePane.getChildren().add(startRect);
        
        return pathStack;
    }
	
	/**
	 * This method will erase the path from the mazePane that is specified by the 
	 * stack of AStarNodes
	 * @param pathStack stack of AStarNodes to be removed from the drawn path
	 */
	private static void erasePath(ArrayDeque<AStarNode> pathStack) {
		for(AStarNode node : pathStack) {
			Point p = node.getPoint();
			if(maze[p.x][p.y] == AStar.Cell.UNBLOCKED) { // Only erase unblocked cells
				Rectangle unblockedRect = new Rectangle(15, 15);
				unblockedRect.setStrokeType(StrokeType.INSIDE);
				unblockedRect.setStroke(Color.LIGHTGREY);
				unblockedRect.setFill(Color.WHITE);
				GridPane.setConstraints(unblockedRect, node.getPoint().y, node.getPoint().x);
				mazePane.getChildren().add(unblockedRect);
			}	
		}
	}
	
	/**
	 * Implementation of repeated forward A*. Open list is sorted by using the given Comparator
	 * @param comparator comparator used to sort the open list.
	 */
	public static void repeatedForwardAStar(Comparator<? super AStarNode> comparator) {
		HashMap<Point, AStarNode> map = new HashMap<Point, AStarNode>();
		numOfExpandedNodes = 0;
		
		Point startPoint = AStar.getStartPoint(maze);
		Point goalPoint = AStar.getGoalPoint(maze);
		
		int manhattanDistance = AStar.getManhattanDistance(startPoint, goalPoint);
		AStarNode start = new AStarNode(startPoint, 0, manhattanDistance, manhattanDistance, 0, null);
		AStarNode goal = new AStarNode(goalPoint, Integer.MAX_VALUE, 0, Integer.MAX_VALUE, 0, null);
		map.put(startPoint, start);
		map.put(goalPoint, goal);
		
		counter = 1;
		start.setgValue(0);
		start.setSearchValue(counter);
		goal.setgValue(Integer.MAX_VALUE);
		goal.setSearchValue(counter);
		
		// Ties in f values are broken using the given comparator
		PriorityQueue<AStarNode> openList = new PriorityQueue<AStarNode>(maze.length * maze[0].length, comparator);
		
		HashSet<AStarNode> closedList = new HashSet<AStarNode>();
		start.setfValue(AStar.getManhattanDistance(start.getPoint(), goal.getPoint()));
		openList.add(start);
		AStar.computePath(openList, closedList, goal, counter, maze, map);
		numOfExpandedNodes += closedList.size();
		if(openList.isEmpty()) {
			System.out.println("Cannot reach target");
		    System.out.println(filename + " Number of expanded nodes: " + numOfExpandedNodes);
			Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
			alert.showAndWait();
			return;
		}
		
		// Draw path and store for agent to traverse
		pathStack = drawPath(goal, start);
		
		// Move agent along path one cell at a time
		Timeline timeline = new Timeline();
		timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(.25), event -> {
			if(pathStack.isEmpty()) { // The agent has reached the goal
				System.out.println("Number of expanded nodes: " + numOfExpandedNodes);
				timeline.stop();
			} else {
				// Make agent attempt to take a step
				AStarNode curr = pathStack.pop();
				boolean tookStep = takeStep(curr);
				
				if(!tookStep) { // Agent couldn't take step because cell was blocked
					counter++;
					AStarNode newStart = map.get(agentPoint);
					newStart.setgValue(0);
					newStart.setSearchValue(counter);
					goal.setgValue(Integer.MAX_VALUE);
					goal.setSearchValue(counter);
					openList.clear();
					closedList.clear();
					newStart.setfValue(newStart.gethValue());
					openList.add(newStart);
					AStar.computePath(openList, closedList, goal, counter, maze, map);
					numOfExpandedNodes += closedList.size();
					erasePath(pathStack);
					
					if(openList.isEmpty()) { // No possible path to target
						System.out.println("Cannot reach target");
						System.out.println("Number of expanded nodes: " + numOfExpandedNodes);
						Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
						//alert.show();
						timeline.stop();
					} else {
						// Draw new path and provide new path for agent
						pathStack = drawPath(goal, newStart);
					}
				}
			}
	    }));
		
		// Make the timeline loop until agent reached target or there are no possible paths
	    timeline.setCycleCount(Animation.INDEFINITE);
	    timeline.play();
	    System.out.println(filename + " Number of expanded nodes: " + numOfExpandedNodes);
	}
	
	/**
	 * Implementation of repeated backward A*. Open list is sorted by using the given Comparator
	 * @param comparator comparator used to sort the open list.
	 */
	public static void repeatedBackwardAStar(Comparator<? super AStarNode> comparator) {
		
		HashMap<Point, AStarNode> map = new HashMap<Point, AStarNode>();
		numOfExpandedNodes = 0;
		
		Point startPoint = AStar.getGoalPoint(maze);
		Point goalPoint = AStar.getStartPoint(maze);
		
		int manhattanDistance = AStar.getManhattanDistance(startPoint, goalPoint);
		AStarNode start = new AStarNode(startPoint, 0, manhattanDistance, manhattanDistance, 0, null);
		AStarNode goal = new AStarNode(goalPoint, Integer.MAX_VALUE, 0, Integer.MAX_VALUE, 0, null);
		map.put(startPoint, start);
		map.put(goalPoint, goal);
		
		counter = 1;
		start.setgValue(0);
		start.setSearchValue(counter);
		goal.setgValue(Integer.MAX_VALUE);
		goal.setSearchValue(counter);
		
		// Ties in f values are broken using the given comparator
		PriorityQueue<AStarNode> openList = new PriorityQueue<AStarNode>(maze.length * maze[0].length, comparator);
		
		HashSet<AStarNode> closedList = new HashSet<AStarNode>();
		start.setfValue(AStar.getManhattanDistance(start.getPoint(), goal.getPoint()));
		openList.add(start);
		AStar.computePath(openList, closedList, goal, counter, maze, map);
		numOfExpandedNodes += closedList.size();
		if(openList.isEmpty()) {
			System.out.println("Cannot reach target");
		    System.out.println(filename + " Number of expanded nodes: " + numOfExpandedNodes);
			Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
			alert.showAndWait();
			return;
		}
		
		// Draw path and store for agent to traverse
		pathStack = drawPathBackwards(goal, start);
		
		// Move agent along path one cell at a time
		Timeline timeline = new Timeline();
		timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(.25), event -> {
			if(pathStack.isEmpty()) { // The agent has reached the goal
				System.out.println("Number of expanded nodes: " + numOfExpandedNodes);
				timeline.stop();
			} else {
				// Make agent attempt to take a step
				AStarNode curr = pathStack.pollLast();
				boolean tookStep = takeStep(curr);
				
				if(!tookStep) { // Agent couldn't take step because cell was blocked
					counter++;
					AStarNode newGoal = map.get(agentPoint);
					newGoal.setgValue(Integer.MAX_VALUE);
					newGoal.setSearchValue(counter);
					start.setgValue(0);
					start.setSearchValue(counter);
					openList.clear();
					closedList.clear();
					openList.add(start);
					AStar.computePath(openList, closedList, newGoal, counter, maze, map);
					numOfExpandedNodes += closedList.size();
					erasePath(pathStack);
					
					if(openList.isEmpty()) { // No possible path to target
						System.out.println("Cannot reach target");
						System.out.println("Number of expanded nodes: " + numOfExpandedNodes);
						Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
						alert.show();
						timeline.stop();
					} else {
						// Draw new path and provide new path for agent
						pathStack = drawPathBackwards(newGoal, start);
					}
				}
			}
	    }));
		
		// Make the timeline loop until agent reached target or there are no possible paths
	    timeline.setCycleCount(Animation.INDEFINITE);
	    //timeline.play();
	    System.out.println(filename + " Number of expanded nodes: " + numOfExpandedNodes);
	}	
	
	/**
	 * Implementation of adaptive A*. Open list is sorted by using the given Comparator
	 * @param comparator comparator used to sort the open list.
	 */
	public static void adaptiveAStar(Comparator<? super AStarNode> comparator) {
		HashMap<Point, AStarNode> map = new HashMap<Point, AStarNode>();
		numOfExpandedNodes = 0;
		
		Point startPoint = AStar.getStartPoint(maze);
		Point goalPoint = AStar.getGoalPoint(maze);
		
		int manhattanDistance = AStar.getManhattanDistance(startPoint, goalPoint);
		AStarNode start = new AStarNode(startPoint, 0, manhattanDistance, manhattanDistance, 0, null);
		AStarNode goal = new AStarNode(goalPoint, Integer.MAX_VALUE, 0, Integer.MAX_VALUE, 0, null);
		map.put(startPoint, start);
		map.put(goalPoint, goal);
		
		counter = 1;
		start.setgValue(0);
		start.setSearchValue(counter);
		goal.setgValue(Integer.MAX_VALUE);
		goal.setSearchValue(counter);
		
		// Ties in f values are broken using the given comparator
		PriorityQueue<AStarNode> openList = new PriorityQueue<AStarNode>(maze.length * maze[0].length, comparator);
		
		HashSet<AStarNode> closedList = new HashSet<AStarNode>();
		start.setfValue(AStar.getManhattanDistance(start.getPoint(), goal.getPoint()));
		openList.add(start);
		AStar.computePath(openList, closedList, goal, counter, maze, map);
		numOfExpandedNodes += closedList.size();
		if(openList.isEmpty()) {
			System.out.println("Cannot reach target");
			System.out.println("Number of expanded nodes: " + numOfExpandedNodes);
			Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
			alert.showAndWait();
		}
		
		// Draw path and store for agent to traverse
		pathStack = drawPath(goal, start);
		
		// Move agent along path one cell at a time
		Timeline timeline = new Timeline();
		timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(.25), event -> {
			if(pathStack.isEmpty()) { // The agent has reached the goal
				System.out.println("Number of expanded nodes: " + numOfExpandedNodes);
				timeline.stop();
			} else {
				// Make agent attempt to take a step
				AStarNode curr = pathStack.pop();
				boolean tookStep = takeStep(curr);
				
				if(!tookStep) { // Agent couldn't take step because cell was blocked
					counter++;
					AStarNode newStart = map.get(agentPoint);
					newStart.setgValue(0);
					newStart.setSearchValue(counter);
					goal.setgValue(Integer.MAX_VALUE);
					goal.setSearchValue(counter);
					openList.clear();
					closedList.clear();
					newStart.setfValue(newStart.gethValue());
					openList.add(newStart);
					AStar.computePath(openList, closedList, goal, counter, maze, map);
					numOfExpandedNodes += closedList.size();
					erasePath(pathStack);
					
					// Update heuristics of expanded nodes
					for(AStarNode node : closedList) {
						node.sethValue(goal.getgValue() - node.getgValue());
					}
					
					if(openList.isEmpty()) { // No possible path to target
						System.out.println("Cannot reach target.");
						System.out.println("Number of expanded nodes: " + numOfExpandedNodes);
						Alert alert = new Alert(AlertType.INFORMATION, "Cannot reach target.", ButtonType.OK);
						alert.show();
						timeline.stop();
					} else {
						// Draw new path and provide new path for agent
						pathStack = drawPath(goal, newStart);
					}
				}
			}
	    }));
		
		// Make the timeline loop until agent reached target or there are no possible paths
	    timeline.setCycleCount(Animation.INDEFINITE);
	    timeline.play();
	}
	
	/**
	 * This method will make the agent take one step on the mazePane
	 * @param node the node that the agent is attempting to enter
	 * @return True if the agent was able to take the step. 
	 * False if the agent wasn't able to take the step.
	 */
	private static boolean takeStep(AStarNode node) {
		Point p = node.getPoint();
		
		// Don't allow agent to step into blocked cells
		if(maze[p.x][p.y] == AStar.Cell.BLOCKED) {
			return false;			
		}
		
		// Update agent's old cell
		maze[p.x][p.y] = AStar.Cell.AGENT;
		Rectangle agentRect = new Rectangle(15, 15);
		agentRect.setStrokeType(StrokeType.INSIDE);
		agentRect.setStroke(Color.LIGHTGREY);
		agentRect.setFill(Color.BLUE);
		GridPane.setConstraints(agentRect, p.y, p.x);
		mazePane.getChildren().add(agentRect);
		
		// Update agent's new cell
		maze[agentPoint.x][agentPoint.y] = AStar.Cell.UNBLOCKED;
		Rectangle unblockedRect = new Rectangle(15, 15);
		unblockedRect.setStrokeType(StrokeType.INSIDE);
		unblockedRect.setStroke(Color.LIGHTGREY);
		unblockedRect.setFill(Color.RED);
		GridPane.setConstraints(unblockedRect, agentPoint.y, agentPoint.x);
		mazePane.getChildren().add(unblockedRect);
		agentPoint = p;
		
		return true;
	}
	
	/**
	 * This method will draw the maze onto the screen
	 */
	private static void drawMaze() {
		// Clear mazePane
		mazePane.getChildren().removeAll(mazePane.getChildren());
		
		// Add new maze to mazePane
		for(int i = 0; i < maze.length; i++) {
			for(int j = 0; j < maze[0].length; j++) {
				Rectangle rect = new Rectangle(15, 15);
				rect.setStroke(Color.LIGHTGREY);
				rect.setStrokeType(StrokeType.INSIDE);
				
				// Set rect to correct color
				if(maze[i][j] == AStar.Cell.UNBLOCKED) {
					rect.setFill(Color.WHITE);
				} else if (maze[i][j] == AStar.Cell.AGENT) {
					rect.setFill(Color.BLUE);
				} else if (maze[i][j] == AStar.Cell.TARGET) {
					rect.setFill(Color.GREEN);
				} else {
					rect.setFill(Color.BLACK);
				}
				
				// update rect on screen
				GridPane.setConstraints(rect, j, i);
				mazePane.getChildren().add(rect);
			}
		}
	}
	
	/**
	 * This method will add a blocking cell to the maze where the
	 * user clicked and update the mazePane.
	 * @param x the x coordinate of the user's click
	 * @param y the y coordinate of the user's click
	 */
	private static void addBlockingCell(double x, double y) {
		// Get indexes of cell that contains the given coordinates
		int rectRow = (int) (y / 15);
		int rectCol = (int) (x / 15);

		if(maze[rectRow][rectCol] != AStar.Cell.TARGET && maze[rectRow][rectCol] != AStar.Cell.AGENT) {
			// Update the maze and mazePane with the newly blocked cell
			maze[rectRow][rectCol] = AStar.Cell.BLOCKED;
			Rectangle blockedRectangle = new Rectangle(15, 15);
			blockedRectangle.setStroke(Color.LIGHTGREY);
			blockedRectangle.setStrokeType(StrokeType.INSIDE);
			blockedRectangle.setFill(Color.BLACK);
			GridPane.setConstraints(blockedRectangle, rectCol, rectRow);
			mazePane.getChildren().add(blockedRectangle);	
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
