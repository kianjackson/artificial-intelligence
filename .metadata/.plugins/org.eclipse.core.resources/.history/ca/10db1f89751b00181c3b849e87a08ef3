import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Scanner;

public class AStar {
	
	// Enum to represent the state of each cell in the grid.
	public static enum Cell {
		UNBLOCKED, BLOCKED, AGENT, TARGET
	}
	
	
	private static final HashMap<String, Cell> CELL_MAP;
    static
    {
        CELL_MAP = new HashMap<String, Cell>();
        CELL_MAP.put("UNBLOCKED", Cell.UNBLOCKED);
        CELL_MAP.put("BLOCKED", Cell.BLOCKED);
        CELL_MAP.put("AGENT", Cell.AGENT);
        CELL_MAP.put("TARGET", Cell.TARGET);
    }
	
	/**
	 * Randomly generates a grid-world with obstacles by using 
	 * depth first search and random tie-breaking.
	 * @param height Height of the grid maze
	 * @param width Width of the grid maze
	 * @return The generated maze
	 */
	public static Cell[][] generateMaze(int height, int width) throws IOException {
		Cell[][] maze = new Cell[height][width];
		boolean[][] visited = new boolean[height][width];

		// Pick random cell to start from
		Random generator = new Random();
		int row = generator.nextInt(height);
		int col = generator.nextInt(width);
		visited[row][col] = true;
		maze[row][col] = Cell.UNBLOCKED;		
		
		Deque<Point> stack = new ArrayDeque<Point>();
		stack.push(new Point(row, col));
		
		while(getUnvistedCell(visited) != null) { // Repeat until all cells have been visited
			Point p = getUnvistedCell(visited);
			stack.push(p);
			visited[p.x][p.y] = true;
			maze[p.x][p.y] = Cell.UNBLOCKED;
			
			while(!stack.isEmpty()) {
				Point curr = stack.pop();
				
				// Pick a random unblocked and unvisited neighbor
				ArrayList<Point> unvisitedNeighbors = getUnvisitedNeighbors(curr.x, curr.y, visited);
				if(unvisitedNeighbors.isEmpty()) { // Dead end
					continue;
				}
				Point neighbor = unvisitedNeighbors.get(generator.nextInt(unvisitedNeighbors.size()));
				
				visited[neighbor.x][neighbor.y] = true;
				if(generator.nextInt(10) < 3) { // Mark cell as blocked with 30% probability
					maze[neighbor.x][neighbor.y] = Cell.BLOCKED;
				} else {
					maze[neighbor.x][neighbor.y] = Cell.UNBLOCKED;
					stack.push(neighbor);	
				}
			}
		}

		// Save generated maze into a text file
		int i;

		//create text file named maze(#).txt where # is from 1 to 50.
		for(i = 1; i <= 50; i++){
		    try {
                File file = new File("maze" + i + ".txt");
                boolean isNew = file.createNewFile();
                if (isNew){
                    System.out.println("Text file for maze "+ i +" has been successfully created!");
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(i == 51){
		    System.out.println("There are already 50 generated maze text files.");
        }
        else{
            String fileName = "maze" + i + ".txt";
            String firstline = width + " " + height + " ";
            try {
                saveMaze(fileName,maze,firstline, i);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

		return maze;
	}

    /** Save the maze into a file with the name "maze#.txt"
     * where # ranges from 1 to 50.
     * @param filename name of the file to save width, height and maze
     * @param maze the string of 10,201 (101 x 101) cells
     */
    private static void saveMaze(String filename, Cell[][] maze, String firstline, int n) throws IOException{
	        FileWriter fileWriter = new FileWriter(filename);
	        PrintWriter printWriter = new PrintWriter(fileWriter);
	        printWriter.print(firstline);
            String stringMaze = "";
            String tempString = "";
            for(int i = 0; i < maze.length; i++) {
                for(int j = 0; j < maze[0].length; j++) {
                    tempString = "" + maze[i][j];
                    stringMaze+= tempString + " ";
                }
                stringMaze += "\n";
            }
	        printWriter.println(stringMaze);
            printWriter.close();
            System.out.println("Maze "+ n + " has been successfully saved!");
            return;
    }

	/**
	 * Loads the maze from the specified file. First 2 tokens
	 * in file must be number of rows and number of columns
	 * @param filename name of the file to load maze from
	 * @return the maze created from the specified file
	 */
	public static Cell[][] loadMaze(String filename) {
		Scanner scanner;
		try {
			scanner = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		int rows = scanner.nextInt();
		int cols = scanner.nextInt();
		Cell[][] maze = new Cell[rows][cols];
		
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) {
				maze[i][j] = CELL_MAP.get(scanner.next());
			}
		}
		
		scanner.close();

		return maze;
	}

    /**
     * Deletes a maze text file
     * @param filename name of the file to delete
     */
    private static void deleteMaze(String filename) {
        File file = new File(filename);

        if(file.delete()){
            System.out.println(filename+ " successfully deleted.");
        }
        else{
            System.out.println("Failed to delete "+ filename);
        }
        return;
    }

	/**
	 * Creates a list of the index of unvisited neighbors of the given element
	 * @param row index of the current element
	 * @param col column of the current element
	 * @param visited 2D array to check whether neighbors are visited or not
	 * @return Returns a list of the coordinates of unvisited neighbors.
	 */
	private static ArrayList<Point> getUnvisitedNeighbors(int row, int col, boolean[][] visited) {
		ArrayList<Point> univisitedNeighbors = new ArrayList<Point>();
		
		if(isInBounds(row + 1, col, visited) && !visited[row+1][col]) {
			univisitedNeighbors.add(new Point(row+1, col));
		}
		if(isInBounds (row - 1, col, visited) && !visited[row-1][col]) {
			univisitedNeighbors.add(new Point(row-1, col));
		}
		if(isInBounds (row, col + 1, visited) && !visited[row][col+1]) {
			univisitedNeighbors.add(new Point(row, col+1));
		}
		if(isInBounds (row, col - 1, visited) && !visited[row][col-1]) {
			univisitedNeighbors.add(new Point(row, col-1));
		}
		
		return univisitedNeighbors;
	}
	
	/**
	 * Returns whether or not the given indexes are in the bounds of the 2D array
	 * @param row row index to check
	 * @param col column index to check
	 * @param grid 2D array to check the bounds on.
	 * @return True if the indexes are in bounds. False otherwise.
	 */
	private static boolean isInBounds(int row, int col, boolean[][] grid) {
		return row >= 0 && row < grid.length && col >= 0 && col < grid[0].length; 
	}
	
	private static boolean isInBounds(int row, int col, Cell[][] maze) {
		return row >= 0 && row < maze.length && col >= 0 && col < maze[0].length; 
	}
	
	/**
	 * Finds the location of an unvisited cell in the given visited array
	 * @param visited the 2D array that holds whether or not a cell has been visited
	 * @return The position of an unvisited cell. Returns null if all cells are visited.
	 */
	private static Point getUnvistedCell(boolean[][] visited) {
		for(int i = 0; i < visited.length; i++) {
			for(int j = 0; j < visited[0].length; j++) {
				if(!visited[i][j]) {
					return new Point(i, j);
				}
			}
		}
		
		return null;
	}
	
	private static void printMaze(Cell[][] maze) {
		for(int i = 0; i < maze.length; i++) {
			for(int j = 0; j < maze[0].length; j++) {
				System.out.print(maze[i][j] + " ");
			}
			System.out.println();
		}
	}
	
<<<<<<< HEAD

=======
>>>>>>> 5514c8fccb14f0464b7be5eaa4f7928cae607dc8
	/**
	 * 
	 * @param openList openList of nodes to possibly expand
	 * @param closedList list of expanded nodes
	 * @param goal the goal node
	 * @param counter the counter
	 * @param maze the maze
	 * @param map map of Points to their corresponding AStarNodes
	 */
	public static void computePath(PriorityQueue<AStarNode> openList, HashSet<AStarNode> closedList, AStarNode goal, int counter, Cell[][] maze, HashMap<Point, AStarNode> map) {
		while(openList.peek() != null && goal.getgValue() > openList.peek().getfValue()) {
			AStarNode s = openList.poll();
			closedList.add(s);
			HashSet<Point> successorStates = getSuccessorStates(s.getPoint(), closedList, maze);
			for(Point p : successorStates) {
				if(!map.containsKey(p)) {
					map.put(p, new AStarNode(p));
				}
				AStarNode a = map.get(p);
				if(a.getSearchValue() < counter) {
					a.setgValue(Integer.MAX_VALUE);
					a.setSearchValue(counter);
				}
				
				if(a.getgValue() > s.getgValue() + 1) {
					a.setgValue(s.getgValue() + 1 );
					a.setParent(s);
					if(openList.contains(a)) {
						openList.remove(a);
					}
					a.setfValue(a.getgValue() + getManhattanDistance(a.getPoint(), goal.getPoint()));
					openList.add(a);
				}	
			}
		}
	}
	
	/**
	 * Gets the possible successor states that the agent can reach with one action
	 * from it's current position
	 * @param p the current coordinates of the agent
	 * @param maze the maze that the agent is in
	 * @return the set of successor states that the agent can reach with one action
	 */
	private static HashSet<Point> getSuccessorStates(Point p, HashSet<AStarNode> closedList, Cell[][] maze) {
		HashSet<Point> successorStates = new HashSet<Point>();
		if(isInBounds(p.x + 1, p.y, maze) && !containsPoint(new Point(p.x + 1, p.y), closedList) && maze[p.x + 1][p.y] != Cell.BLOCKED) {
			successorStates.add(new Point(p.x + 1, p.y));
		}
		if(isInBounds (p.x - 1, p.y, maze) && !containsPoint(new Point(p.x - 1, p.y), closedList) && maze[p.x - 1][p.y] != Cell.BLOCKED) {
			successorStates.add(new Point(p.x - 1, p.y));
		}
		if(isInBounds (p.x, p.y + 1, maze) && !containsPoint(new Point(p.x, p.y + 1), closedList) && maze[p.x][p.y + 1] != Cell.BLOCKED) {
			successorStates.add(new Point(p.x, p.y + 1));
		}
		if(isInBounds (p.x, p.y - 1, maze) && !containsPoint(new Point(p.x, p.y - 1), closedList) && maze[p.x][p.y - 1] != Cell.BLOCKED) {
			successorStates.add(new Point(p.x, p.y - 1));
		}
		
		return successorStates;
	}
	
	/**
	 * Determines whether or not there is an AStarNode in the given set
	 * that has the given coordinates
	 * @param p the coordinates to search for
	 * @param set set of AStarNodes to search in 
	 * @return True if there is an AStarNode with the given coordinates. False otherwise.
	 */
	private static boolean containsPoint(Point p, HashSet<AStarNode> set) {
		for(AStarNode node : set) {
			if(node.getPoint().equals(p)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets the Manhattan distance between two points
	 * @param p1 the first point
	 * @param p2 the second point
	 * @return the manhattan distance between p1 and p2
	 */
	public static int getManhattanDistance(Point p1, Point p2) {
		return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y);
	}
	
	/**
	 * Gets the coordinates of the agent in the maze
	 * @param maze the maze to search for the agent in
	 * @return the coordinates of the agent. Returns null if no agent is found.
	 */
	public static Point getStartPoint(Cell[][] maze) {
		for(int i = 0; i < maze.length; i++) {
			for(int j = 0; j < maze[0].length; j++) {
				if(maze[i][j] == Cell.AGENT) {
					return new Point(i, j);
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Gets the coordinates of the target in the maze
	 * @param maze the maze to search for the target in
	 * @return the coordinates of the target. Returns null if no target is found.
	 */
	public static Point getGoalPoint(Cell[][] maze) {
		for(int i = 0; i < maze.length; i++) {
			for(int j = 0; j < maze[0].length; j++) {
				if(maze[i][j] == Cell.TARGET) {
					return new Point(i, j);
				}
			}
		}
		
		return null;
	}
	
	public static void main(String[] args) throws IOException {
		//for(int i = 0; i < 50)
	    //this loop creates all of the mazes
//	    for(int i = 0; i < 50; i++) {
//			try {
//				AStar.generateMaze(101, 101);
//			} catch (IOException e) {
//				e.printStackTrace();
//				return;
//			}
//		}
		 

        //this loop deletes all of the mazes
//        String filename = "";
//        for(int i = 1; i <= 50; i++){
//            filename = "maze" + i + ".txt";
//            AStar.deleteMaze(filename);
//        }
		
		//generateMaze(5, 5);
		//repeatedForwardAStar(loadMaze("maze1.txt"));
	}
}
