# naiveBayes.py
# -------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

import util
import classificationMethod
import math

class NaiveBayesClassifier(classificationMethod.ClassificationMethod):
  """
  See the project description for the specifications of the Naive Bayes classifier.
  
  Note that the variable 'datum' in this code refers to a counter of features
  (not to a raw samples.Datum).
  """
  def __init__(self, legalLabels):
    self.legalLabels = legalLabels
    self.type = "naivebayes"
    self.k = 1 # this is the smoothing parameter, ** use it in your train method **
    self.automaticTuning = False # Look at this flag to decide whether to choose k automatically ** use this in your train method **
    
  def setSmoothing(self, k):
    """
    This is used by the main method to change the smoothing parameter before training.
    Do not modify this method.
    """
    self.k = k

  def train(self, trainingData, trainingLabels, validationData, validationLabels):
    """
    Outside shell to call your method. Do not modify this method.
    """  
      
    # might be useful in your code later...
    # this is a list of all features in the training set.
    self.features = list(set([ f for datum in trainingData for f in datum.keys() ]));
    
    if (self.automaticTuning):
        kgrid = [0.001, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 20, 50]
    else:
        kgrid = [self.k]
        
    self.trainAndTune(trainingData, trainingLabels, validationData, validationLabels, kgrid)
      
  def trainAndTune(self, trainingData, trainingLabels, validationData, validationLabels, kgrid):
    """
    Trains the classifier by collecting counts over the training data, and
    stores the Laplace smoothed estimates so that they can be used to classify.
    Evaluate each value of k in kgrid to choose the smoothing parameter 
    that gives the best accuracy on the held-out validationData.
    
    trainingData and validationData are lists of feature Counters.  The corresponding
    label lists contain the correct label for each datum.
    
    To get the list of all possible features or labels, use self.features and 
    self.legalLabels.
    """

    "*** YOUR CODE HERE ***"
    # Initialize Counters for features
    featureCounter = {}
    for feature in self.features:
        featureCounter[feature] = {
            0: util.Counter(),
            1: util.Counter()
        }

    # Count features in trainingData
    for i in range(len(trainingData)):
        label = trainingLabels[i] 
        for feature, value in trainingData[i].items():
            featureCounter[feature][value][label] += 1

    # Estimate P(Y) from the trainingData
    self.priorDistribution = util.Counter()
    for label in trainingLabels:
        self.priorDistribution[label] += 1.0

    for label in self.priorDistribution:
        self.priorDistribution[label] /= len(trainingLabels)

    # Initialize counters for conditional probabilities
    conditionalProbabilities = {}
    for feature in self.features:
        conditionalProbabilities[feature] = util.Counter()

    # This is where the conditional probabilities of the best k-value will be stored
    self.conditionalProbabilities = {}

    highestCorrectGuesses = 0
    mostAccurateConiditionalProbabilities = {}

    # Evaluate each value of k in kgrid 
    for k in kgrid:
        # Initialize counters for conditional probabilities 
        conditionalProbabilities = {}
        for feature in self.features:
            conditionalProbabilities[feature] = {
                0: util.Counter(),
                1: util.Counter()
            }

        # Estimate conditional probabilities with Laplace smoothing
        for label in self.legalLabels:
            for feature in self.features:
                for value in range(0,2):
                    conditionalProbabilities[feature][value][label] = (featureCounter[feature][value][label] + k) / (featureCounter[feature][0][label] + featureCounter[feature][1][label] + (2.0 * k))

        # Check classifier using validation data to choose the best k-value
        correctGuesses = 0
        self.conditionalProbabilities = conditionalProbabilities
        guesses = self.classify(validationData)
        for i in range(len(guesses)):
            if guesses[i] == validationLabels[i]:
                correctGuesses += 1

        # Update k-value and conditional probabilities if new k-value is more accurate 
        if correctGuesses > highestCorrectGuesses:
            self.k = k
            mostAccurateConiditionalProbabilities = conditionalProbabilities
            highestCorrectGuesses = correctGuesses
        elif correctGuesses == highestCorrectGuesses:
            if k < self.k:
                self.k = k
                mostAccurateConiditionalProbabilities = conditionalProbabilities

    self.conditionalProbabilities = mostAccurateConiditionalProbabilities
        
  def classify(self, testData):
    """
    Classify the data based on the posterior distribution over labels.
    
    You shouldn't modify this method.
    """
    guesses = []
    self.posteriors = [] # Log posteriors are stored for later data analysis (autograder).
    for datum in testData:
      posterior = self.calculateLogJointProbabilities(datum)
      guesses.append(posterior.argMax()) 
      self.posteriors.append(posterior) 
    return guesses
      
  def calculateLogJointProbabilities(self, datum):
    """
    Returns the log-joint distribution over legal labels and the datum.
    Each log-probability should be stored in the log-joint counter, e.g.    
    logJoint[3] = <Estimate of log( P(Label = 3, datum) )>
    
    To get the list of all possible features or labels, use self.features and 
    self.legalLabels.
    """

    "*** YOUR CODE HERE ***"
    logJoint = util.Counter()
    for label in self.legalLabels:
        logJoint[label] = math.log(self.priorDistribution[label])
        for feature in self.conditionalProbabilities:
            if self.conditionalProbabilities[feature][datum[feature]][label] != 0: # math.log(0) causes a ValueError
                logJoint[label] += math.log(self.conditionalProbabilities[feature][datum[feature]][label])
    
    return logJoint
  
  def findHighOddsFeatures(self, label1, label2):
    """
    Returns the 100 best features for the odds ratio:
            P(feature=1 | label1)/P(feature=1 | label2) 
    
    Note: you may find 'self.features' a useful way to loop through all possible features
    """
    featuresOdds = []
       
    "*** YOUR CODE HERE ***"
    for feature in self.features:
        featuresOdds.append((feature, self.conditionalProbabilities[feature][1][label1] / self.conditionalProbabilities[feature][1][label2]))
        featuresOdds.sort(key=lambda tup: tup[1])

    return [f[0] for f in featuresOdds[0:-100]]
    